# Experiment Data

This repository contains the manually annotated trees, the generated
trees and logfiles for the experiments described in the paper [to be
published].

## Preparation Sentences

The directory [prep_sentences](prep_sentences) contains the tree data
for the sentences used to create the initial ruleset.  The sentences
are available in the CDA format used by
the [jwcdg parser](https://gitlab.com/nats/jwcdg) for use with the
AnnoViewer, which was used in the manual annotation process; as well
as in the CoNLL format.

Also included are the script used to pick the test sentences as well
as a small script used to automatically convert a few edges for easier
manual annotation.

## Validation / Test Sentences

The [validation_sentences](validation_sentences) contains the 50
randomly chosen sentences used to test the [rulefile](rulefile.tud) as
well as the log output of the program used for comparing the manually
annotated and generated files and the output of the generation process.

## Coverage Experiment

The coverage experiment data was not uploaded as it did not contain
manual annotation data.  To replicate the experiment, download the
Hamburg Dependeny Treebank files
from [here](http://hdl.handle.net/11022/0000-0000-7FC7), run the
transducer on part_B and check for coverage with the TrUDucer.

Log output used for the calculations in the paper:

```
16:30:36 INFO  Main - 1570471 nodes converted correctly.
16:30:36 INFO  Main - 234884 nodes punctuation.
16:30:36 INFO  Main - 145785 nodes not converted.
```
