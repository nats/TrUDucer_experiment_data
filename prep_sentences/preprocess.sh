#! /bin/bash

dir=test_sentences_ud_preprocessed/

map="AVZ,mark CJ,conj DET,det GMOD,nmod KON,cc PN,nmod PP,case S,root SUBJ,nsubj"

for file in $(ls $dir); do
    for mapping in $map; do
        oldIFS=$IFS
        IFS=","
        set $mapping
        echo "s/-> '"$1"'/-> '"$2"'/g"
        sed -i "s/-> '"$1"'/-> '"$2"'/g" $dir/$file
        IFS=$oldIFS
    done
done
