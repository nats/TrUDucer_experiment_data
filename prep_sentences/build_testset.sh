#! /bin/bash

# pipe set of deprels into the script

sentsPerDeprel=3
listOfDeprels=deprels.list
dirOfSentences=part_A/
targetDir=test_sentences/
tempDir=tmp/

mkdir -p $tempDir $targetDir

for deprel in $(cat $listOfDeprels); do
    echo "Looking at $deprel"
    grep -r "'SYN' -> '$deprel'" $dirOfSentences | sed 's/:.*//g' | sort | uniq > $tempDir/$deprel.list
    echo "Sentences with $deprel:" $(wc -l $tempDir/$deprel.list)
    cat $tempDir/$deprel.list | head -n $sentsPerDeprel | xargs -I fn cp fn $targetDir
done
